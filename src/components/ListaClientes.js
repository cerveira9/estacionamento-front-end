import React, { Component } from 'react';
import axios from 'axios';
import Header from './Header.js'
import Breadcrumb from './Breadcrumb.js'
import '../App.css';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
class Atendimento extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            clientes: [],
            loading: true
        };
    }

    componentDidMount() {
        this.getRequest();
    }

    getRequest() {
        axios
            .get('http://localhost:8080/estacionamento/rest/ws/getClientes/')
            .then(res =>
                this.setState({ 
                    clientes: res.data, 
                    loading: false 
                }),
            );
    }

    renderClientes() {
        console.log('entrou no render clientes');
        console.log('this.state.clientes', this.state.clientes);
        return this.state.clientes.map((valor, i) => (
            <li key={i}>{valor.idCliente}</li>
        ))
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="body">
                    <Breadcrumb title="Clientes"/>
                    <DataTable value={this.state.clientes}>                      
                        <Column field="nomeDoCliente" header="Cliente" />
                        <Column field="cpfDoCliente" header="CPF" />
                        <Column field="cpfDoCliente" header="RG" />
                    </DataTable>
                </div>

            </div>
        );
    }
}

export default Atendimento;